<?php

namespace BureauHouse\Modules\Core\Entities;

class Input extends Entity
{
    protected $table = 'input';

    protected $fillable = ['name', 'description', 'label', 'placeholder', 'type'];

    /**
     * {@inheritDoc}
     */
    protected $icon = 'fa-th';

    public function modules()
    {
        return $this->belongsToMany('BureauHouse\Modules\Core\Entities\Module', 'module_input', 'input_id', 'module_id')
            ->using('BureauHouse\Modules\Core\Entities\Pivot\ModuleInput')
            ->withPivot(['is_required']);
    }

    public function sections()
    {
        return $this->belongsToMany('BureauHouse\Modules\Core\Entities\Section', 'module_section', 'input_id', 'module_id')
            ->using('BureauHouse\Modules\Core\Entities\Pivot\SectionInput');
    }
}
