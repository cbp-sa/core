<?php

namespace BureauHouse\Modules\Core\Entities;

class Field extends Entity
{
    const ACCOUNT_NUMBER = 'AccountNumber';
    const APPOINTMENT_DATE = 'AppointmentDate';
    const AGE = 'Age';
    const AUDITOR_NAME = 'AuditorName';
    const AUDITOR_STATUS = 'AuditorStatus';
    const AMOUNT = 'Amount';
    const BUSINESS_NAME = 'BusinessName';
    const BUSINESS_ADDRESS_LINE1 = 'BusinessAddressLine1';
    const COMMENTS = 'Comments';
    const CONTACT_PERSON = 'ContactPerson';
    const CONTACT_INFORMATION = 'ContactInformation';
    const DATE_OF_BIRTH = 'DateOfBirth';
    const DATE_OF_DEATH = 'DateOfDeath';
    const DEBT_COUNCILLOR = 'DebtCouncillor';
    const DEBT_COUNCILLOR_CONTACT = 'DebtCouncillorContact';
    const DIRECTOR_STATUS = 'DirectorStatus';
    const DIRECTOR_TYPE = 'DirectorType';
    const EMAIL_ADDRESS = 'EmailAddress';
    const EMPLOYER_NAME = 'EmployerName';
    const EMPLOYER_REGISTRATION_NUMBER = 'EmployerRegistrationNumber';
    const ENTERPRISE_NAME = 'EnterpriseName';
    const ESTATE_NUMBER = 'EstateNumber';
    const EXECUTOR_ADDRESS = 'ExecutorAddress';
    const EXECUTOR_NAME = 'ExecutorName';
    const FIRST_NAME = 'FirstName';
    const FIRST_STATUS = 'FirstStatus';
    const ID_NUMBER = 'IDNumber';
    const LATEST_DATE = 'LatestDate';
    const LATEST_STATUS = 'LatestStatus';
    const LINKS = 'Links';
    const PASSPORT = 'Passport';
    const PERSON_NAME = 'PersonName';
    const PLACE_OF_DEATH = 'PlaceOfDeath';
    const OCCUPATION = 'Occupation';
    const PROFESSION_NUMBER = 'ProfessionNumber';
    const MAIDEN_NAME = 'MaidenName';
    const MARITAL_STATUS = 'MaritalStatus';
    const NETWORK = 'Network';
    const RECORD_DATE = 'RecordDate';
    const REFERENCE = 'Reference';
    const REGISTER_ENTRY_DATE = 'RegisterEntryDate';
    const REGISTRATION_NUMBER = 'RegistrationNumber';
    const RESIGNATION_DATE = 'ResignationDate';
    const POSTAL_ADDRESS_LINE1 = 'PostalAddressLine1';
    const SUBSCRIBER = 'Subscriber';
    const SCORE = 'Score';
    const STATUS = 'Status';
    const STATUS_CODE = 'StatusCode';
    const STATUS_DESCRIPTION = 'StatusDescription';
    const SURVIVING_SPOUSE_ID_NUMBER = 'SurvivingSpouseIDNumber';
    const SURNAME = 'Surname';
    const TEL_NUMBER = 'TelNumber';
    const TEL_TYPE = 'TelType';
    const ORIGINAL_ADDRESS = 'OriginalAddress';

    protected $table = 'field';
    protected $fillable = ['name', 'description', 'input_id'];

    /**
     * {@inheritDoc}
     */
    protected $icon = 'fa-th';

    /**
     * Get the inputs for the field.
     *
     * @return Collection
     */
    public function input()
    {
        return $this->hasMany('BureauHouse\Modules\Core\Entities\Input', 'input_id');
    }
}
