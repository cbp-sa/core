<?php

namespace BureauHouse\Modules\Core\Entities;

use Illuminate\Support\Str;

class Module extends Entity
{
    protected $table = 'module';
    protected $fillable = ['name', 'icon', 'description', 'uniqueKey', 'is_enabled'];

    public function products()
    {
        return $this->belongsToMany('BureauHouse\Modules\Core\Entities\Product', 'module_product', 'module_id', 'product_id')
            ->using('BureauHouse\Modules\Core\Entities\Pivot\ModuleProduct');
    }

    public function sections()
    {
        return $this->belongsToMany('BureauHouse\Modules\Core\Entities\Section', 'module_section', 'module_id', 'section_id')
            ->using('BureauHouse\Modules\Core\Entities\Pivot\ModuleSection');
    }

    public function inputs()
    {
        return $this->belongsToMany('BureauHouse\Modules\Core\Entities\Input', 'module_input', 'module_id', 'input_id')
            ->using('BureauHouse\Modules\Core\Entities\Pivot\ModuleInput')
            ->withPivot(['is_required']);
    }
}
