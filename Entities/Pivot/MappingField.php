<?php

namespace BureauHouse\Modules\Core\Entities\Pivot;

class MappingField extends SortablePivot
{
    protected $table = 'mapping_field';
}
