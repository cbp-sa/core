<?php

namespace BureauHouse\Modules\Core\Entities\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ModuleSection extends Pivot
{
    protected $table = 'module_section';
}
