<?php

namespace BureauHouse\Modules\Core\Entities\Pivot;

class ModuleProduct extends SortablePivot
{
    protected $table = 'module_product';
}
