<?php

namespace BureauHouse\Modules\Core\Entities\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;

class SectionInput extends Pivot
{

    protected $table = 'section_input';
}
