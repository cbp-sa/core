<?php

namespace BureauHouse\Modules\Core\Entities\Pivot;

class ModuleInput extends SortablePivot
{
    protected $table = 'module_input';
}
