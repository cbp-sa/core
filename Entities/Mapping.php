<?php

namespace BureauHouse\Modules\Core\Entities;

class Mapping extends Entity
{
    protected $table = 'mapping';

    /**
     * {@inheritDoc}
     */
    protected $isManageable = false;

    protected $fillable = ['name', 'type', 'section_id'];

    public function section()
    {
        return $this->hasMany('BureauHouse\Modules\Core\Entities\Section', 'section_id');
    }

    public function module()
    {
        return $this->hasMany('BureauHouse\Modules\Core\Entities\Module', 'module_id');
    }

    public function fields()
    {
        return $this->belongsToMany('BureauHouse\Modules\Core\Entities\Field', 'mapping_field', 'mapping_id', 'field_id')
            ->using('BureauHouse\Modules\Core\Entities\Pivot\MappingField')
            ->withPivot(['label', 'position', 'module_id']);
    }
}
