<?php

namespace BureauHouse\Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Webpatser\Uuid\Uuid;

use BureauHouse\Helpers\Traits\ModelHelper;
use BureauHouse\Helpers\Traits\RelationshipsHelper;

abstract class Entity extends Model
{
    use Traits\ManageableTrait,
        Traits\IconTrait,
        ModelHelper,
        RelationshipsHelper,
        SoftDeletes;

    public function isEnabled()
    {
        return $this->getAttributeValue('is_enabled');
    }

    public static function create(array $attributes = [])
    {
        $attributes = array_merge($attributes, ['uniqueKey' => Uuid::generate(4)->string]);

        return static::query()->create($attributes);
    }
}
