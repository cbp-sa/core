<?php

namespace BureauHouse\Modules\Core\Entities;

use Illuminate\Support\Facades\Auth;

class Favourite extends Entity
{
    protected $table = 'favourite';

    /**
     * {@inheritDoc}
     */
    protected $isManageable = false;

    public static function boot()
    {

        parent::boot();

        static::addGlobalScope(
            function ($query) {
                $query->where('userCode', Auth::user()->userCode);
            }
        );
    }

    public function module()
    {
        return $this->hasMany('BureauHouse\Modules\Core\Entities\Module', 'module_id');
    }
}
