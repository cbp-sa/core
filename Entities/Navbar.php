<?php

namespace BureauHouse\Modules\Core\Entities;

class Navbar extends Entity
{
    protected $table = 'navbar';
    protected $isManageable = false;
}
