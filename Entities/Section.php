<?php

namespace BureauHouse\Modules\Core\Entities;

class Section extends Entity
{
    const AVS = 'AVS';
    const IDV = 'IDV';
    const IDV_SUMMARY = 'IDV Summary';
    const PERSON = 'Person';
    const POLITICIAN = 'Politician';
    const SAFPS = 'SAFPS';
    const SAFPS_PROTECTIVE = 'SAFPS Protective';
    const SPIDER_WEB_RELATIONSHIPS = 'Spider web relationships';
    const STRIKE_DATES = 'Strike dates';

    protected $table = 'section';
    protected $fillable = ['name','uniqueKey', 'description', 'url','callingModule','is_enabled', 'type', 'can_add'];

    public function inputs()
    {
        return $this->belongsToMany('BureauHouse\Modules\Core\Entities\Input', 'section_input', 'section_id', 'input_id')
            ->using('BureauHouse\Modules\Core\Entities\Pivot\SectionInput');
    }

    public function modules()
    {
        return $this->belongsToMany('BureauHouse\Modules\Core\Entities\Module', 'module_section', 'module_id', 'section_id')
            ->using('BureauHouse\Modules\Core\Entities\Pivot\ModuleSection');
    }

    public function mapping()
    {
        return $this->hasMany('BureauHouse\Modules\Core\Entities\Mapping');
    }

    public function dataloader()
    {
        return $this->belongsTo('BureauHouse\Modules\Core\Entities\DataLoader');
    }
}
