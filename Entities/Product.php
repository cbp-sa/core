<?php

namespace BureauHouse\Modules\Core\Entities;

class Product extends Entity
{
    protected $table = 'product';
    protected $fillable = ['name', 'description', 'icon', 'uniqueKey', 'is_enabled', 'product_id'];

    public function modules()
    {
        return $this->belongsToMany('BureauHouse\Modules\Core\Entities\Module', 'module_product', 'product_id', 'module_id')
            ->using('BureauHouse\Modules\Core\Entities\Pivot\ModuleProduct');
    }

    public function products()
    {
        return $this->hasMany('BureauHouse\Modules\Core\Entities\Product');
    }
}
