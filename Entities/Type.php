<?php

namespace BureauHouse\Modules\Core\Entities;

class Type extends Entity
{
    const DATE = 'date';
    const EMAIL = 'email';
    const RANGE = 'range';
    const GRID = 'grid';
    const NUMBER = 'number';
    const SELECT = 'select';
    const TEXT = 'text';

    protected $table = 'types';
    protected $fillable = ['name', 'description'];

    /**
     * {@inheritDoc}
     */
    protected $isManageable = false;
}
