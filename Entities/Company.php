<?php

namespace BureauHouse\Modules\Core\Entities;

class Company extends Entity
{
    protected $table = 'company';

    /**
     * {@inheritDoc}
     */
    protected $icon = 'fa-building';

    protected $fillable = [
        'Title',
        'Name',
        'ShortName',
        'Address',
        'Tel',
        'Fax',
        'EmailSupport',
        'EmailSales',
        'Website'
    ];
}
