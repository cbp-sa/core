<?php

namespace BureauHouse\Modules\Core\Entities\Traits;

trait IconTrait
{
    /**
     * @var string
     */
    protected $icon = 'fa-forward';

    /**
     * icons use per model over Admin API
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
