<?php

namespace BureauHouse\Modules\Core\Entities\Traits;

trait ManageableTrait
{
    /**
     * @var boolean
     */
    protected $isManageable = true;

    /**
     * use to expose model over Admin API
     *
     * @return boolean
     */
    public function isManageable()
    {
        return $this->isManageable;
    }
}
