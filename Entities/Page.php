<?php

namespace BureauHouse\Modules\Core\Entities;

use Cviebrock\EloquentSluggable\Sluggable;

class Page extends Entity
{
    use Sluggable;

    protected $table = 'page';
    protected $fillable = ['title', 'description', 'slug'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
