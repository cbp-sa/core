<?php

namespace BureauHouse\Modules\Core\Entities;

class DataLoader extends Entity
{
    protected $table = 'dataloader';
    protected $fillable = ['url','permission'];
}
