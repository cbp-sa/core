<?php

namespace BureauHouse\Modules\Core\Repositories;

use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BureauHouse\Validators\ActivityValidator;
use Spatie\Activitylog\Models\Activity;
use BureauHouse\Modules\Core\Entities\Module;
use BureauHouse\Modules\Core\Entities\User;

/**
 * Class ActivityRepositoryEloquent.
 *
 * @package namespace BureauHouse\Repositories;
 */
class ActivityRepositoryEloquent extends BaseRepository implements ActivityRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Activity::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function findOneByUserAndModule(User $user, Module $module, array $parameters)
    {
        return $this->findWhere(
            [
                'causer_id' => $user->id,
                'subject_id' => $module->id,
                'description' => md5(serialize($parameters)),
                ['created_at', '>', Carbon::now()->subMinute()],
                ['created_at', '<', Carbon::now()->addMinute()],
            ]
        )
        ->first();
    }
}
