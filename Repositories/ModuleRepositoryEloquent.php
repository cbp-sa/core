<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Traits\CacheableRepository;
use BureauHouse\Modules\Core\Entities\Module;
use BureauHouse\Validators\ModuleValidator;

/**
 * Class ModuleRepositoryEloquent.
 *
 * @package namespace BureauHouse\Repositories;
 */
class ModuleRepositoryEloquent extends BaseRepository implements ModuleRepository, CacheableInterface
{

    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Module::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function findModulesByProductUniqueKey(string $uniqueKey)
    {
        $product = resolve(ProductRepository::class)->findByField('uniqueKey', $uniqueKey)->first();

        return $product->modules()->get();
        //return $this->findByField('product_id', $product->id);
        return $this->with(
            ['products' => function ($query) use ($product) {
                return $query->where('id', '=', $product->id);
            }]
        )->get();//->findByField('uniqueKey', $uniqueKey)->first();
    }
}
