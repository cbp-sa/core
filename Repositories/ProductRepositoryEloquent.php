<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BureauHouse\Modules\Core\Entities\Product;
use BureauHouse\Validators\ProductValidator;

/**
 * Class ProductRepositoryEloquent.
 *
 * @package namespace BureauHouse\Repositories;
 */
class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function findOneByUniqueKeyAndPermissions($uniqueKey, array $permissions = [])
    {
        return $this->with(
            [
                'modules' => function ($moduleQuery) use ($permissions) {
                    return $moduleQuery->orderBy('position', 'asc')->with([
                        'inputs' => function ($inputQuery) {
                            return $inputQuery->orderBy('position', 'asc');
                        },
                        'sections' => function ($sectionQuery) use ($permissions) {
                            return $sectionQuery
                                ->whereIn('permission', $permissions)
                                ->with(['dataloader' => function ($dataloaderQuery) use ($permissions) {
                                    return $dataloaderQuery->whereIn('permission', $permissions);
                                }])
                                ->orderBy('position', 'asc');
                        }
                    ]);
                }
            ]
        )->findByField('uniqueKey', $uniqueKey)->first();
    }

    public function findOneByUniqueKeyWithModule($uniqueKey, $moduleUniqueKey, array $permissions = [])
    {
        return $this->with(
            [
                'modules' => function ($query) use ($moduleUniqueKey, $permissions) {
                    return $query->where('uniqueKey', $moduleUniqueKey)->with([
                        'sections' => function ($sectionQuery) use ($permissions) {
                            return $sectionQuery->whereIn('permission', $permissions)->orderBy('position', 'asc');
                        }
                    ]);
                }
            ]
        )->findByField('uniqueKey', $uniqueKey)->first();
    }
}
