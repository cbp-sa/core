<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BureauHouse\Modules\Core\Repositories\PageRepository;
use BureauHouse\Modules\Core\Entities\Page;
use BureauHouse\Validators\PageValidator;

/**
 * Class PageRepositoryEloquent.
 *
 * @package namespace BureauHouse\Repositories;
 */
class PageRepositoryEloquent extends BaseRepository implements PageRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Page::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
