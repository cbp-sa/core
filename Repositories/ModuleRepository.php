<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ModuleRepository.
 *
 * @package namespace BureauHouse\Repositories;
 */
interface ModuleRepository extends RepositoryInterface
{
    //
}
