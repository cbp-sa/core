<?php

namespace BureauHouse\Modules\Core\Repositories;

use BureauHouse\Modules\Core\Entities\Module;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Traits\CacheableRepository;
use BureauHouse\Modules\Core\Entities\Section;
use BureauHouse\Validators\SectionValidator;

/**
 * Class SectionRepositoryEloquent.
 *
 * @package namespace BureauHouse\Repositories;
 */
class SectionRepositoryEloquent extends BaseRepository implements SectionRepository, CacheableInterface
{

    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Section::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function findOneByModuleAndUniqueKey(Module $module, $uniqueKey)
    {
        return $this->with(
            ['modules' => function ($query) use ($module) {
                return $query->where('id', '=', $module->id);
            }]
        )->findByField('uniqueKey', $uniqueKey)->first();
    }
}
