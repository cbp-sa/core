<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ActivityRepository.
 *
 * @package namespace BureauHouse\Repositories;
 */
interface ActivityRepository extends RepositoryInterface
{
    //
}
