<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MappingRepository.
 *
 * @package namespace BureauHouse\Repositories;
 */
interface MappingRepository extends RepositoryInterface
{
    //
}
