<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use BureauHouse\Modules\Core\Entities\Mapping;
use BureauHouse\Modules\Core\Entities\Module;
use BureauHouse\Validators\MappingValidator;

/**
 * Class MappingRepositoryEloquent.
 *
 * @package namespace BureauHouse\Repositories;
 */
class MappingRepositoryEloquent extends BaseRepository implements MappingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Mapping::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function findOneByNameAndTypeWithModule(string $name, string $type, int $moduleId)
    {
        return $this->with([
            'fields' => function ($query) use ($moduleId) {
                return $query->where(['module_id' => $moduleId]);
            }])->findWhere([
                'name' => $name,
                'type' => $type
        ])->first();
    }
}
