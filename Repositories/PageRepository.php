<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PageRepository.
 *
 * @package namespace BureauHouse\Repositories;
 */
interface PageRepository extends RepositoryInterface
{
    //
}
