<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CompanyRepository.
 *
 * @package namespace BureauHouse\Repositories;
 */
interface CompanyRepository extends RepositoryInterface
{
    //
}
