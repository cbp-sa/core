<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SectionRepository.
 *
 * @package namespace BureauHouse\Repositories;
 */
interface SectionRepository extends RepositoryInterface
{
    //
}
