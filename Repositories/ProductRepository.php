<?php

namespace BureauHouse\Modules\Core\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository.
 *
 * @package namespace BureauHouse\Repositories;
 */
interface ProductRepository extends RepositoryInterface
{
    //
}
