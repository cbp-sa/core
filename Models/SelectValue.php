<?php

namespace BureauHouse\Modules\Core\Models;

class SelectValue
{
    private $selected;
    private $options;

    public function addOption(SelectOption $selectOption)
    {
        $this->options[] = $selectOption->toObject();

        return $this;
    }

    /**
     * Get the value of selected
     */
    public function getSelected()
    {
        return $this->selected;
    }

    /**
     * Set the value of selected
     *
     * @return  self
     */
    public function setSelected($selected)
    {
        $this->selected = $selected;

        return $this;
    }

    public function toArray()
    {
        return [
            'selected' => $this->selected,
            'options' => $this->options,
        ];
    }

    public function __toString()
    {
        return json_encode((object) $this->toArray());
    }
}
