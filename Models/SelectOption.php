<?php

namespace BureauHouse\Modules\Core\Models;

use stdClass;

class SelectOption
{
    private $value;
    private $text;

    /**
     * Get the value of value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set the value of value
     *
     * @return  self
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get the value of text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set the value of text
     *
     * @return  self
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    public function toArray()
    {
        return [
            'value' => $this->value,
            'text' => $this->text,
        ];
    }

    public function toObject()
    {
        return ((object) $this->toArray());
    }
}
