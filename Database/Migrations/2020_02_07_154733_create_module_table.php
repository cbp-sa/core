<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'module',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name')->unique();
                $table->uuid('uniqueKey');
                $table->string('description')->nullable();
                $table->string('icon')->nullable();
                $table->boolean('is_enabled')->default(true);
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module');
    }
}
