<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInputFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_field', function (Blueprint $table) {
            $table->bigInteger('input_id')->unsigned();
            $table->bigInteger('field_id')->unsigned();

            $table->foreign('input_id')->references('id')->on('input');
            $table->foreign('field_id')->references('id')->on('field');

            $table->unique(['input_id', 'field_id']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_field');
    }
}
