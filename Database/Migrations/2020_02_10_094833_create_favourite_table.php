<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFavouriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'favourite',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('userCode');
                $table->bigInteger('module_id')->unsigned();
                $table->bigInteger('module_use_count');
                $table->timestamps();

                $table->foreign('module_id')->references('id')->on('module');
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favourite');
    }
}
