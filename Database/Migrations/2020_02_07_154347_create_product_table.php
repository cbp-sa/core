<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'product',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->uuid('uniqueKey');
                $table->string('icon');
                $table->string('description')->nullable();
                $table->bigInteger('position')->unsigned()->default(0);
                $table->boolean('is_enabled')->default(true);
                $table->bigInteger('product_id')->unsigned()->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->unique(['name', 'product_id']);
                $table->foreign('product_id')->references('id')->on('product');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
