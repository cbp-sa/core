<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavbarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(
            'navbar',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('label');
                $table->string('uri')->unique();
                $table->bigInteger('type_id')->unsigned();
                $table->string('role');
                $table->string('permission')->nullable();
                $table->string('class')->nullable();
                $table->string('icon')->nullable();
                $table->boolean('is_new')->default(true);
                $table->boolean('is_treeview')->default(false);
            
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('type_id')->references('id')->on('types');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('navbar');
    }
}
