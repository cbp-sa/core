<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionInputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_input', function (Blueprint $table) {

            $table->bigInteger('section_id')->unsigned();
            $table->bigInteger('input_id')->unsigned();

            $table->foreign('section_id')->references('id')->on('section');
            $table->foreign('input_id')->references('id')->on('input');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_input');
    }
}
