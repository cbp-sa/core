<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMappingFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapping_field', function (Blueprint $table) {
            $table->bigInteger('mapping_id')->unsigned();
            $table->bigInteger('field_id')->unsigned();
            $table->bigInteger('module_id')->unsigned();
            $table->bigInteger('position')->unsigned()->default(0);
            $table->string('label')->nullable();

            $table->foreign('mapping_id')->references('id')->on('mapping');
            $table->foreign('field_id')->references('id')->on('field');
            $table->foreign('module_id')->references('id')->on('module');

            $table->unique(['mapping_id', 'field_id', 'module_id']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mapping_field');
    }
}
