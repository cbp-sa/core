<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->uuid('uniqueKey');
            $table->string('description');
            $table->string('url');
            $table->string('callingModule');
            $table->string('permission')->nullable();
            $table->string('type')->nullable();
            $table->boolean('is_enabled');
            $table->boolean('can_add')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->bigInteger('dataloader_id')->unsigned()->nullable();
            $table->foreign('dataloader_id')->references('id')->on('dataloader');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section');
    }
}
