<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleInputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_input', function (Blueprint $table) {

            $table->bigInteger('module_id')->unsigned();
            $table->bigInteger('input_id')->unsigned();
            $table->bigInteger('position')->unsigned()->default(0);
            $table->boolean('is_required')->default(0);

            $table->foreign('module_id')->references('id')->on('module');
            $table->foreign('input_id')->references('id')->on('input');

            $table->unique(['module_id', 'input_id']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_input');
    }
}
