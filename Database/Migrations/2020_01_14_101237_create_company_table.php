<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'company',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('Title');
                $table->uuid('uniqueKey');
                $table->integer('CaptchaLimit');
                $table->integer('CaptchaTime');
                $table->string('ShortName', 50)->unique();
                $table->string('Name', '500')->unique();
                $table->string('Tel', '20');
                $table->string('Fax', '20');
                $table->string('Address', '500');
                $table->string('EmailSupport', '200');
                $table->string('EmailSales', '200');
                $table->string('EmailAccess', '200');
                $table->string('Website', '500');
                $table->string('RegistrationNumber', '50');
                $table->string('NCRNumber', '50');
                $table->string('VATNumber', '50');
                $table->string('Directors', '2000');
                $table->string('Logo', '200');
                $table->string('DashboardImage', '200');
                $table->string('Footprint', '2000');
                $table->string('DisclaimerNotice', '2000');
                $table->string('SystemType', '50');
                $table->string('VendorID', '50');
                $table->string('VendorPassword', '50');
                $table->boolean('is_enabled')->default(0);
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
