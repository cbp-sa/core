<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_product', function (Blueprint $table) {

            $table->bigInteger('module_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('position')->unsigned()->default(0);

            $table->foreign('module_id')->references('id')->on('module');
            $table->foreign('product_id')->references('id')->on('product');

            $table->unique(['module_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_product');
    }
}
