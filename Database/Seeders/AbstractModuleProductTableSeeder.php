<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use BureauHouse\Modules\Core\Entities\Module;
use BureauHouse\Modules\Core\Entities\Product;

abstract class AbstractModuleProductTableSeeder extends SortableSeeder
{
    protected function getTableName()
    {
        return 'module_product';
    }

    protected function getValues(Module $module, Product $product)
    {
        return [
            'module_id' => $module->id,
            'product_id' => $product->id,
        ];
    }
}
