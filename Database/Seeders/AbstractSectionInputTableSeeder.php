<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use BureauHouse\Modules\Core\Entities\Input;
use BureauHouse\Modules\Core\Entities\Section;

abstract class AbstractSectionInputTableSeeder extends Seeder
{
    protected function getTableName()
    {
        return 'section_input';
    }

    protected function getValues(Section $section, Input $input)
    {
        return [
            'section_id' => $section->id,
            'input_id' => $input->id,
        ];
    }
}
