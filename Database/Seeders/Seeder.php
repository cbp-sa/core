<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder as BaseSeeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;
use Illuminate\Database\QueryException;

use BureauHouse\Modules\Core\Entities\DataLoader;
use BureauHouse\Modules\Core\Entities\Field;
use BureauHouse\Modules\Core\Entities\Input;
use BureauHouse\Modules\Core\Entities\Mapping;
use BureauHouse\Modules\Core\Entities\Module;
use BureauHouse\Modules\Core\Entities\Product;
use BureauHouse\Modules\Core\Entities\Section;

abstract class Seeder extends BaseSeeder
{
    private $dataloaders = [];
    private $fields = [];
    private $inputs = [];
    private $mappings = [];
    private $modules = [];
    private $products = [];
    private $sections = [];

    /**
     * Try inserting product
     *
     * @param string $tableName
     * @param array $values
     * @return void
     */
    protected function tryInsert(array $values = [])
    {
        try {
            DB::table($this->getTableName())->insert($values);
        } catch (QueryException $e) {
            Log::error($e->getMessage());
        }

        return $this;
    }

    protected function getModuleByName(string $name)
    {
        if (isset($this->modules[$name])) {
            return $this->modules[$name];
        }
        $module = Module::where('name', '=', $name)->first();
        $this->modules[$name] = $module;

        return $module;
    }

    protected function getMappingByName(string $name)
    {
        if (isset($this->mappings[$name])) {
            return $this->mappings[$name];
        }
        $mapping = Mapping::where('name', '=', $name)->first();
        $this->mappings[$name] = $mapping;

        return $mapping;
    }

    protected function getFieldByName(string $name)
    {
        if (isset($this->fields[$name])) {
            return $this->fields[$name];
        }
        $field = Field::where('name', '=', $name)->first();
        $this->fields[$name] = $field;

        return $field;
    }

    protected function getSectionByName(string $name)
    {
        if (isset($this->sections[$name])) {
            return $this->sections[$name];
        }
        $section = Section::where('name', '=', $name)->first();
        $this->sections[$name] = $section;

        return $section;
    }

    protected function getDataLoaderByName(string $name)
    {
        if (isset($this->dataloaders[$name])) {
            return $this->dataloaders[$name];
        }
        $dataloader = DataLoader::where('name', '=', $name)->first();
        $this->dataloaders[$name] = $dataloader;

        return $dataloader;
    }

    protected function getInputByName($name)
    {
        if (isset($this->inputs[$name])) {
            return $this->inputs[$name];
        }
        $input = Input::where('name', '=', $name)->first();
        $this->inputs[$name] = $input;

        return $input;
    }

    protected function getProductByName($name)
    {
        if (isset($this->products[$name])) {
            return $this->products[$name];
        }
        $product = Product::where('name', '=', $name)->first();
        $this->products[$name] = $product;

        return $product;
    }

    abstract protected function getTableName();
}
