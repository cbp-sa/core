<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;

use BureauHouse\Modules\Core\Entities\Input;
use BureauHouse\Modules\Core\Entities\Type;
use BureauHouse\Modules\Core\Models\SelectValue;
use stdClass;

abstract class AbstractInputTableSeeder extends Seeder
{

    /**
     * {@inheritDoc}
     */
    protected function getTableName()
    {
        return 'input';
    }

    private function getLabel(string $name)
    {
        $title = str_replace('_', ' ', Str::snake($name));

        return Str::title($title);
    }

    protected function getValues(string $name, string $placeholder, ?string $pattern = null, string $type = 'text')
    {
        return [
            'name' => $name,
            'uniqueKey' => Uuid::generate(4)->string,
            'label' => $this->getLabel($name),
            'placeholder' => $placeholder,
            'type' => $type,
            'pattern' => $pattern,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];
    }

    protected function setRangeValues(array $values, int $minimum, int $maximum)
    {
        $values['minimum'] = $minimum;
        $values['maximum'] = $maximum;

        return $values;
    }

    protected function setSelectValues(array $values, SelectValue $selectValue)
    {
        $values['select_values'] = (string) ($selectValue);

        return $values;
    }
}
