<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

use BureauHouse\Entities\Field;

abstract class AbstractFieldTableSeeder extends Seeder
{
    protected function getTableName()
    {
        return 'field';
    }

    protected function getValues(string $name, string $description)
    {
        return [
            'name' => $name,
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => $description,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];
    }
}
