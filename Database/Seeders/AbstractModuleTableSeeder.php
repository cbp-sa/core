<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

use BureauHouse\Modules\Core\Entities\Module;

abstract class AbstractModuleTableSeeder extends Seeder
{

    protected function getTableName()
    {
        return 'module';
    }

    protected function getValues(string $name, ?string $icon = null, ?string $description = null)
    {
        return [
            'name' => $name,
            'uniqueKey' => Uuid::generate(4)->string,
            'icon' => $icon,
            'description' => $description,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];
    }
}
