<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use BureauHouse\Modules\Core\Entities\Field;
use BureauHouse\Modules\Core\Entities\Mapping;
use BureauHouse\Modules\Core\Entities\Module;

abstract class AbstractMappingFieldTableSeeder extends SortableSeeder
{

    private $module;
    private $mapping;

    protected function getTableName()
    {
        return 'mapping_field';
    }

    protected function getValues(Field $field, ?string $label = null)
    {
        return [
            'mapping_id' => $this->mapping->id,
            'module_id' => $this->module->id,
            'field_id' => $field->id,
            'label' => $label,
        ];
    }

    /**
     * Set the value of module
     *
     * @return  self
     */
    protected function setModule(Module $module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Set the value of mapping
     *
     * @return  self
     */
    public function setMapping(Mapping $mapping)
    {
        $this->mapping = $mapping;

        return $this;
    }
}
