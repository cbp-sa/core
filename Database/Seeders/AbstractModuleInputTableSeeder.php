<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use BureauHouse\Modules\Core\Entities\Input;
use BureauHouse\Modules\Core\Entities\Module;

abstract class AbstractModuleInputTableSeeder extends SortableSeeder
{

    /**
     * {@inheritDoc}
     */
    protected function getTableName()
    {
        return 'module_input';
    }

    protected function getValues(Module $module, Input $input, $isRequired = true)
    {
        return [
            'module_id' => $module->id,
            'input_id' => $input->id,
            'is_required' => $isRequired,
        ];
    }
}
