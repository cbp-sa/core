<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use Carbon\Carbon;

use BureauHouse\Modules\Core\Entities\Section;

abstract class AbstractMappingTableSeeder extends Seeder
{
    protected function getTableName()
    {
        return 'mapping';
    }

    protected function getValues(string $name, string $type, Section $section)
    {
        return [
            'name' => $name,
            'type' => $type,
            'section_id' => $section->id,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];
    }
}
