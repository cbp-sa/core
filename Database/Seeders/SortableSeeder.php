<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

abstract class SortableSeeder extends Seeder
{
    private $position = 0;
    private $override = false;

    protected function resetPosition()
    {
        $this->position = 0;
        $this->override = false;

        return $this;
    }

    protected function setPosition(int $position = 0)
    {
        $this->override = true;
        $this->position = $position;

        return $this;
    }

    private function incrementPosition()
    {
        ++$this->position;

        return $this;
    }

    protected function getPosition()
    {
        return $this->position;
    }

    /**
     * {@inheritDoc}
     */
    protected function tryInsert(array $values = [])
    {
        if (!$this->override) {
            $this->incrementPosition();
        }
        $values['position'] = $this->getPosition();

        return parent::tryInsert($values);
    }
}
