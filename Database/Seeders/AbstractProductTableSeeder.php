<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use BureauHouse\Modules\Core\Entities\Product;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

abstract class AbstractProductTableSeeder extends SortableSeeder
{
    private $product;

    protected function getTableName()
    {
        return 'product';
    }

    /**
     * {@inheritDoc}
     */
    protected function tryInsert(array $values = [])
    {
        $values['position'] = $this->getPosition();

        return parent::tryInsert($values);
    }

    protected function getValues(string $name)
    {
        return [
            'name' => $name,
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => null,
            'product_id' => $this->product ? $this->product->id : null,
            'icon' => 'fa fa-th',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];
    }

    /**
     * Set the value of product
     *
     * @return  self
     */
    protected function setParentProduct($product)
    {
        $this->product = $product;

        return $this;
    }
}
