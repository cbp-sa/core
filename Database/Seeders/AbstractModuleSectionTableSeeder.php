<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use BureauHouse\Modules\Core\Entities\Module;
use BureauHouse\Modules\Core\Entities\Section;

class AbstractModuleSectionTableSeeder extends SortableSeeder
{
    protected function getTableName()
    {
        return 'module_section';
    }

    protected function getValues(Module $module, Section $section)
    {
        return [
            'module_id' => $module->id,
            'section_id' => $section->id,
        ];
    }
}
