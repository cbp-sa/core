<?php

namespace BureauHouse\Modules\Core\Database\Seeders;

use BureauHouse\Modules\Core\Entities\DataLoader;

use Carbon\Carbon;

abstract class AbstractDataLoaderTableSeeder extends Seeder
{
    protected function getTableName()
    {
        return 'dataloader';
    }

    protected function getValues(string $name, string $uri, string $permission)
    {
        return [
            'name' => $name,
            'uri' => $uri,
            'permission' => $permission,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ];
    }
}
