<?php

namespace BureauHouse\Modules\Core\Providers;

use BureauHouse\Modules\Core\Repositories\ActivityRepository;
use BureauHouse\Modules\Core\Repositories\ActivityRepositoryEloquent;
use BureauHouse\Modules\Core\Repositories\CompanyRepository;
use BureauHouse\Modules\Core\Repositories\CompanyRepositoryEloquent;
use BureauHouse\Modules\Core\Repositories\MappingRepository;
use BureauHouse\Modules\Core\Repositories\MappingRepositoryEloquent;
use BureauHouse\Modules\Core\Repositories\ModuleRepository;
use BureauHouse\Modules\Core\Repositories\ModuleRepositoryEloquent;
use BureauHouse\Modules\Core\Repositories\PageRepository;
use BureauHouse\Modules\Core\Repositories\PageRepositoryEloquent;
use BureauHouse\Modules\Core\Repositories\ProductRepository;
use BureauHouse\Modules\Core\Repositories\ProductRepositoryEloquent;
use BureauHouse\Modules\Core\Repositories\SectionRepository;
use BureauHouse\Modules\Core\Repositories\SectionRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CompanyRepository::class, CompanyRepositoryEloquent::class);
        $this->app->bind(MappingRepository::class, MappingRepositoryEloquent::class);
        $this->app->bind(ModuleRepository::class, ModuleRepositoryEloquent::class);
        $this->app->bind(ProductRepository::class, ProductRepositoryEloquent::class);
        $this->app->bind(SectionRepository::class, SectionRepositoryEloquent::class);
        $this->app->bind(PageRepository::class, PageRepositoryEloquent::class);
        $this->app->bind(ActivityRepository::class, ActivityRepositoryEloquent::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
